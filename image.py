from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

img = Image.open("01.jpg")
draw = ImageDraw.Draw(img)

textwidth, textheight = draw.textsize(text, font)


# font = ImageFont.truetype(<font-file>, <font-size>)
font = ImageFont.truetype("Crimson-Bold.otf", 16)
# draw.text((x, y),"Sample Text",(r,g,b))
draw.text((10, 0),"Sample Text",(255,255,255),font=font)
img.save('01.jpg')