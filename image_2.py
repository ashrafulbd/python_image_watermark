from PIL import Image, ImageDraw, ImageFont
 
image = Image.open('01.jpg')
width, height = image.size
 
draw = ImageDraw.Draw(image)
text = "A watermark"
 
font = ImageFont.truetype('Crimson-Bold.otf', 20)
textwidth, textheight = draw.textsize(text, font)
 
# calculate the x,y coordinates of the text
margin = 100
x = width - textwidth - margin
y = height - textheight - margin
 
# draw watermark in the bottom right corner
draw.text((x, y), text, font=font)
 
image.save('02.jpg')